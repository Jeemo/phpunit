<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 6/2/2018
	 * Time: 5:13 PM
	 */
	
	namespace Tests\AppBundle\Service;
	
	
	use AppBundle\Entity\Dinosaur;
	use AppBundle\Entity\Enclosure;
	use AppBundle\Factory\DinosaurFactory;
	use AppBundle\Service\EnclosureBuilderService;
	use Doctrine\ORM\EntityManagerInterface;
	use PHPUnit\Framework\TestCase;
	use Prophecy\Argument;
	
	/**
	 * This version of EnclosureBuilderServiceTest utilizes the
	 * Prophecy Mocking library instead of the default PHPUnit one.
	 *
	 * It is included with PHPUnit.
	 *
	 * Class EnclosureBuilderServiceProphecyTest
	 * @package Tests\AppBundle\Service
	 */
	class EnclosureBuilderServiceProphecyTest extends TestCase
	{
		public function testItBuildsAndPersistsEnclosure() {
			$em = $this->prophesize(EntityManagerInterface::class);
			$em->persist(Argument::type(Enclosure::class))
				->shouldBeCalledTimes(1);
			
			$em->flush()->shouldBeCalled();
			
			$dinoFactory = $this->prophesize(DinosaurFactory::class);
			$dinoFactory
				->growFromSpecification(Argument::type('string'))
				->willReturn(new Dinosaur())
				->shouldBeCalledTimes(2);
				
			$builder = new EnclosureBuilderService(
				$em->reveal(),
				$dinoFactory->reveal()
			);
			
			$enclosure = $builder->buildEnclosure(1, 2);
			
			$this->assertCount(1, $enclosure->getSecurities());
			$this->assertCount(2, $enclosure->getDinosaurs());
		}
	}