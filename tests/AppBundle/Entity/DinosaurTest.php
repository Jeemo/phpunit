<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/11/2018
	 * Time: 1:27 PM
	 */

	namespace Tests\AppBundle\Entity;


	use AppBundle\Entity\Dinosaur;
	use PHPUnit\Framework\TestCase;

	class DinosaurTest extends TestCase
	{
		public function testSettingLength() {
			$dinosaur = new Dinosaur();
			$this->assertSame(0, $dinosaur->getLength());

			$dinosaur->setLength(9);
			$this->assertSame(9, $dinosaur->getLength());
		}

		public function testDinosaurHasNotShrunk() {
			$dinosaur = new Dinosaur();
			$dinosaur->setLength(15);

			$this->assertGreaterThan(12, $dinosaur->getLength(), 'Did you put it in the washing machine?');
		}

		public function testReturnsFullSpecificationOfDinosaur() {
			$dinosaur = new Dinosaur();

			$this->assertSame(
				'Unknown: non-carnivorous dinosaur that is 0 meters long.',
				$dinosaur->getSpecification()
			);
		}

		public function testReturnsFullSpecificationForTyrannosaurus() {
			$dinosaur = new Dinosaur('Tyrannosaurus', true);
			$dinosaur->setLength(12);

			$this->assertSame(
				'Tyrannosaurus: carnivorous dinosaur that is 12 meters long.',
				$dinosaur->getSpecification()
			);
		}
	}