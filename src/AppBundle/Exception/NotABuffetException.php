<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/22/2018
	 * Time: 1:36 PM
	 */
	
	namespace AppBundle\Exception;
	
	
	class NotABuffetException extends \Exception
	{
		protected $message = "Please do not mix the carnivorous and non-carnivorous dinosaurs.  It makes a mess!";
	}