<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dinosaurs")
 */
class Dinosaur
{
	const LARGE = 10;
	const HUGE = 30;
	
	/**
	 * @var Enclosure
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enclosure", inversedBy="dinosaurs")
	 */
	private $enclosure;
	/**
   * @ORM\Column(type="integer")
   */
  private $length = 0;

	/**
	 * @ORM\Column(type="string")
	 */
	private $genus;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isCarnivorous;

  public function __construct(string $genus = 'Unknown', bool $isCarnivorous = false)  {
	  $this->genus = $genus;
		$this->isCarnivorous = $isCarnivorous;
  }

	public function getLength(): int
	{
		return $this->length;
	}

	public function setLength(int $length)
	{
		$this->length = $length;
	}

	public function getSpecification() {
   	return sprintf('%s: %scarnivorous dinosaur that is %d meters long.',
			$this->genus,
			$this->isCarnivorous ? '' : 'non-',
			$this->getLength()
		);
	}

	public function getGenus() {
   	return $this->genus;
	}

	/**
	 * @return mixed
	 */
	public function isCarnivorous()
	{
		return $this->isCarnivorous;
	}
}
