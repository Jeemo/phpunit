<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/22/2018
	 * Time: 1:00 PM
	 */
	
	namespace AppBundle\Entity;
	
	use AppBundle\Exception\DinosaursAreRunningRampantException;
	use AppBundle\Exception\NotABuffetException;
	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\Common\Collections\Collection;
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity
	 * @ORM\Table(name="enclosure")
	 */
	class Enclosure
	{
		/**
		 * @var ArrayCollection
		 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Dinosaur", mappedBy="enclosure", cascade={"persist"})
		 */
		private $dinosaurs;
		
		/**
		 * @var ArrayCollection|Security[]
		 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security", mappedBy="enclosure", cascade={"persist"})
		 */
		private $securities;
		
		/**
		 * Enclosure constructor.
		 * @param bool $withBasicSecurity
		 */
		public function __construct(bool $withBasicSecurity = false) {
			$this->dinosaurs = new ArrayCollection();
			$this->securities = new ArrayCollection();
			
			if ($withBasicSecurity) {
				$this->addSecurity(new Security('Fence', true, $this));
			}
		}
		
		/**
		 * @return ArrayCollection
		 */
		public function getDinosaurs(): ArrayCollection {
			return $this->dinosaurs;
		}
		
		/**
		 * @param Dinosaur $dinosaur
		 * @throws NotABuffetException
		 * @throws DinosaursAreRunningRampantException
		 */
		public function addDinosaur(Dinosaur $dinosaur) {
			if (!$this->canAddDinosaur($dinosaur)) {
				throw new NotABuffetException();
			}
			if (!$this->isSecurityActive()) {
				throw new DinosaursAreRunningRampantException('Are you craaazy?!!?');
			}
			$this->dinosaurs[] = $dinosaur;
		}
		
		/**
		 * @param Security $security
		 */
		public function addSecurity(Security $security) {
			$this->securities[] = $security;
		}
		
		/**
		 * @return bool
		 */
		public function isSecurityActive(): bool {
			foreach ($this->securities as $security) {
				if ($security->getIsActive()) {
					return true;
				}
			}
			
			return false;
		}
		
		public function getSecurities(): Collection {
			return $this->securities;
		}
		/**
		 * @param Dinosaur $dinosaur
		 * @return bool
		 */
		private function canAddDinosaur(Dinosaur $dinosaur): bool {
			return count($this->dinosaurs) === 0
				|| $this->dinosaurs->first()->isCarnivorous() === $dinosaur->isCarnivorous();
		}
		
	}